import yfinance as yf
import pandas as pd
import sys

for i in range(1, len(sys.argv)):
    yticker = yf.Ticker(sys.argv[i])
    data = yticker.history(period="7d", interval="15m")
    data.to_csv('data/' + sys.argv[i] + '.csv')
