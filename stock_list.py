from iexfinance.refdata import get_symbols
from datetime import datetime
import pandas as pd
from iexfinance.stocks import Stock

symbols = get_symbols()
symbols = symbols.symbol.to_list()

sym = []
x = 0
n = 100

for i in range(x, len(symbols), n):
    x = i
    sym.append(symbols[x:x+n])

format = '%d_%m_%Y_%H_%M_%S'
symbols = sym

result = pd.DataFrame()
for symbol in symbols:
    stock = Stock(symbol)
    price = stock.get_price()
    price = price.transpose()
    result = result.append(price.query('price < 10'))

result.to_csv('data/stock_' + datetime.now().strftime(format) + '.csv',

