from iexfinance.stocks import get_historical_intraday
from datetime import datetime
import pandas as pd
import sys

date = datetime.today()
filename = 'data/stock_10_02_2021_19_08_01.csv'
#filename = sys.argv[1]

symbols = pd.read_csv(filename)
sym = symbols[:10].symbol.to_list()
result = pd.DataFrame()
for s in sym:
    hist = get_historical_intraday(s, date, output_format='pandas')
    hist = hist.dropna(subset=['marketOpen'])
    per = (hist.marketClose - hist.marketOpen) / hist.marketOpen * 100
    hist.insert(loc=len(hist.columns), column='per', value=per)

    print(s)
    if (len(hist) > 0):
        hist.insert(0, column='symbol', value=s)
        result = result.append(hist)

format = '%d_%m_%Y_%H_%M_%S'
result.to_csv('data/historical_' + datetime.now().strftime(format) + '.csv')


